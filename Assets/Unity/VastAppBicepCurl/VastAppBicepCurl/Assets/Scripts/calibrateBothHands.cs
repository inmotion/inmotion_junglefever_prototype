using UnityEngine;
using System.Collections;

public class calibrateBothHands : MonoBehaviour {
	
	calibration rightHand, leftHand;
	public TextMesh textM;
	public float startTime;
	
	float timer, timeStamp;
	bool newTimer, playGame;
	
	void Start () {
		leftHand = GameObject.Find("leftHandCalibration").GetComponent<calibration>();
		rightHand = GameObject.Find("rightHandCalibration").GetComponent<calibration>();
	}
	
	// Update is called once per frame
	void Update () {
		textM.text = "Calibrate hands";
		if(!playGame)
		{
			if(leftHand.isWithin && !rightHand.isWithin)
			{
				textM.text = "Left hand calirated, place right hand in box";
					timeStamp = Time.time + startTime;
			}
			if(rightHand.isWithin && !leftHand.isWithin)
			{
				textM.text = "Right hand calibrated, place left hand in box";
					timeStamp = Time.time + startTime;
			}
			if(leftHand.isWithin && rightHand.isWithin)
			{
				//Set timer
				if(!newTimer)
				{
					timeStamp = Time.time + startTime;
					newTimer = true;
				}
				
				//Increment timer
				timer = timeStamp - Time.time;
				
				//Display timer
				textM.text = Mathf.CeilToInt( timer ).ToString();
	
				if(timer <= 0)
				{
					playGame = true;
	
				}
			
			}
		}
		else
		{
			newTimer = false;
			playGame = false;
			
		}
		
		if(playGame)
		{
			GameObject.Find ("GameStateController").GetComponent<GameStateController>().EndCalibrationStartGame();
			textM.text = "Calibrated!";
			
			//Make sure uneeded code is not ran
			playGame = false;
			
			//Delete uneeded objects
			if(leftHand != null)
				Destroy(leftHand.gameObject);
			
			if(leftHand != null)
				Destroy(rightHand.gameObject);
			if(textM != null)
				Destroy(textM.gameObject, 1f);
			Destroy(gameObject);
		}
	}
}
