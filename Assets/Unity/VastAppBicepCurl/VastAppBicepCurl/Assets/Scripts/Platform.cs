using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {
	
	public int angle;
	Transform left, right;
	void Start()
	{
		left = GameObject.Find("Player2Cursor").transform;
		right = GameObject.Find("Player1Cursor").transform;
	}
	void Update () {
		//transform.LookAt(right);
		if((right.position.y - left.position.y > 50) && ( angle >= 315 || angle < 45 ))
		{
			transform.Rotate(Vector3.forward,1f);
		}
		if((left.position.y - right.position.y > 50) && ( angle > 315 || angle <= 45 ))
		{
			transform.Rotate(Vector3.forward,-1f);
		}
		
		angle = (int) transform.rotation.eulerAngles.z ;
	}
}
