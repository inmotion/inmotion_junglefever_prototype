using UnityEngine;
using System.Collections;

public class Basket : MonoBehaviour {
	
	Vector3 startScale;
	public bool amSelected;
	void Start () {
		startScale = transform.localScale;
	}
	
	public void Select()
	{
		transform.localScale = startScale + new Vector3(10f,10f,10f);
		amSelected = true;
	
	}
	public void Deselect()
	{
		transform.localScale = startScale;
		amSelected = false;
		
	}
}
