using UnityEngine;
using System.Collections;

public class BasketSelector : MonoBehaviour {
	public Platform myPlatform;
	public Basket basketRed, basketBlue;
	public bool isRedSelected, isPlatformThere;
	void Start()
	{
		isPlatformThere = false;
	}
	void Update () {
		if(isPlatformThere && myPlatform.renderer.enabled == true)
		{
			if(myPlatform.angle > 315)
			{
				SelectBlue();
				isRedSelected = false;
			}
			if(myPlatform.angle < 315)
			{
				SelectRed();
				isRedSelected = true;
			}
		}
	}
	void SelectBlue()
	{
		basketBlue.Select();
		basketRed.Deselect();
	}
	void SelectRed()
	{
		basketRed.Select();
		basketBlue.Deselect();
	}
}
