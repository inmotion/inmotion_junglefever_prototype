using UnityEngine;
using System.Collections;

public class Director : MonoBehaviour {
	
	public GameObject player1;
	public GameObject player2;
	private bool keyboardMode = false;
	
	void Start () {
		SetEnabledState(player2, true);
		BGMotionManager.GetInstance().SetPlayerCount(2);
		BGMotionManager.GetInstance().SetEditorWebcamMode();
		
		player1.GetComponent<CursorMovement>().SetCameraTransform(transform);
		player2.GetComponent<CursorMovement>().SetCameraTransform(transform);
	}
	
	public void SetEnabledState(GameObject target, bool enabledState) {
		CursorMovement cm = target.GetComponent<CursorMovement>() as CursorMovement;
		if(enabledState)
			BGMotionManager.GetInstance().AddBodyReadingListener(cm);
		else
			BGMotionManager.GetInstance().RemoveBodyReadingListener(cm);
			
		//Sets visibility for an object and all its children
		target.SetActiveRecursively(enabledState);
		Renderer[] renderers = target.GetComponentsInChildren<Renderer>();
		foreach (Renderer r in renderers) {
			r.enabled = enabledState;
		}
	}
}
