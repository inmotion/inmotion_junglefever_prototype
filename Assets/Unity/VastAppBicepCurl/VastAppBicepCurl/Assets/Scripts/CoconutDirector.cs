using UnityEngine;
using System.Collections;

public class CoconutDirector : MonoBehaviour
{
	
	public GameObject coconutObj;
	public Transform coconutStartTrans;
	//List for coconuts to be added / removed
	CoconutList list;
	
	//Number of coconuts in the list
	int numberOfCoconuts;
	//Number of coconuts that will be displayed on the GUI
	int GUICoconuts;
	
	//Color to be added to the list
	bool isRed;
	
	//Timer variables
	public float timer, timeStamp, startTime;
	public bool newTimer, shouldUpdate;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Use this for initialization
	void Start()
	{
		isRed = true;
		newTimer = false;
		
		list = GameObject.Find("CoconutList").GetComponent<CoconutList>();
		
		numberOfCoconuts = list.GetCoconutListSize();
		
		//If this int is changed then objects must be added to the scene to accomodate
		GUICoconuts = 5;
		
		//Fill coconut list (there are better ways of doing this code)
		for(int counter = 0; counter < numberOfCoconuts; counter++)
		{
			if(isRed)
			{
				list.PushCoconut(Color.red);
				isRed = false;
			}
			else
			{
				list.PushCoconut(Color.blue);
				isRed = true;
			}
		}
		
		//Display the first five coconuts in the list
		for(int i = 0; i < GUICoconuts; i++)
		{
			GameObject.Find( "renderer" + (i + 1) ).renderer.material.color = list.GetColorAt(i);
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		if(shouldUpdate)
		{
			if(list.GetCoconutListSize() > 0)
			{
				//Set timer
				if(!newTimer)
				{
					timeStamp = Time.time + startTime;
					newTimer = true;
				}
				
				//Increment timer
				timer = timeStamp - Time.time;
				
				//Display timer
				GameObject.Find("timerDisplay").GetComponent<TextMesh>().text = Mathf.CeilToInt( timer ).ToString();
				
				if(timer <= 0 && newTimer == true)
				{
					//Set the timer to be run again
					newTimer = false;
					
					NewCoconut();
				}
			}
		}
	}
	
	void NewCoconut()
	{
		Color newCoconutColor = list.GetColorAt(0);
		//Push after pop in order to not go out of bounds
		if(isRed)
		{	
			//Create new coconut using pop
			list.CreateNewCoconut(Color.red);
			
			isRed = false;
		}
		else
		{
			//Create new coconut using pop
			list.CreateNewCoconut(Color.blue);
			
			isRed = true;
		}
		
		//Change materials for GUI display
		for(int i = 0; i < GUICoconuts; i++)
		{
			GameObject.Find( "renderer" + (i + 1) ).renderer.material.color = list.GetColorAt(i);
		}
		
		GameObject coco = (GameObject) GameObject.Instantiate(coconutObj,coconutStartTrans.position, Quaternion.identity);
		coco.renderer.material.color = newCoconutColor;
	}
}
