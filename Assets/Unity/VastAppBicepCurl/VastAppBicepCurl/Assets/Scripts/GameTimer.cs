using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {
	
	public float timer, timeStamp, startTime;
	public bool newTimer, shouldUpdate;
	TextMesh myMesh;
	void Start()
	{
		newTimer = false;	
		myMesh = GameObject.Find("gameTimerDisplay").GetComponent<TextMesh>();
	}
	void Update()
	{
		if(shouldUpdate)
		{
			//Set timer
			if(!newTimer)
			{
				timeStamp = Time.time + startTime;
				newTimer = true;
			}
			
			//Increment timer
			timer = timeStamp - Time.time;
			
			//Display timer
			myMesh.text = "Time Left : " + Mathf.CeilToInt( timer ).ToString();
			
			if(timer <= 0 && newTimer == true)
			{
				//Set the timer to be run again
				newTimer = false;
				shouldUpdate = false;
				GameObject.Find("GameStateController").GetComponent<GameStateController>().GameOver();
				myMesh.text = "Game Over";
			}
		}
	}
}
