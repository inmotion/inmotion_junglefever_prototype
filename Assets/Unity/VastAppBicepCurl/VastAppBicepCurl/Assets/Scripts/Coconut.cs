using UnityEngine;
using System.Collections;

public class Coconut : MonoBehaviour {
	
	BasketSelector myBasketSelector;
	
	void Start () {
		myBasketSelector = GameObject.Find("BasketSelector").GetComponent<BasketSelector>();
		iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("StartPath"), "time", 2, "easeType", iTween.EaseType.linear));
	}
	
	public void TurnOn()
	{
		gameObject.SetActive(true);
	}
	public void TurnOff()
	{
		gameObject.SetActive(false);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		if(other.gameObject.name == "PlatformCollider")
		{
			if(myBasketSelector.isRedSelected == true)
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("LeftmostPath"), "time", 3, "easeType",iTween.EaseType.linear));
			else
				iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("RightmostPath"), "time", 3, "easeType",iTween.EaseType.linear));
		}
		if(other.GetComponent<Basket>() != null)
		{
			if(other.gameObject.renderer.material.color == gameObject.renderer.material.color)
				GameObject.Find("Score").GetComponent<scoreController>().IncreaseScore();
			else
				GameObject.Find("Score").GetComponent<scoreController>().DecreaseScore();
			Destroy(gameObject);
		}
	}
}
