using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoconutList : MonoBehaviour
{
	int coconutListSize;
	//List of coconuts to be displayed (this may change to more than just a color)
	List<Color> coconutList;
	
	//Constructor
	void Awake()
	{
		coconutListSize = 10;
		coconutList = new List<Color>(coconutListSize);
	}
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	//Add coconuts to the end of the created list
	public void PushCoconut(Color coconutColor)
	{
		coconutList.Add(coconutColor);
	}
	
	//Used to display elements
	public Color GetColorAt(int element)
	{
		return coconutList[element];
	}
	
	//Remove coconuts from the front of the list
	public void CreateNewCoconut(Color newCoconutColor)
	{
		int sizeOfList = coconutList.Count;
		
		//Set coconut colors correctly
		for(int counter = 1; counter < sizeOfList; counter++)
		{
			coconutList[counter - 1] = coconutList[counter];
			
			//For debugging purposes
			Debug.Log(" [ " + (counter - 1) + " ] = " + coconutList[counter - 1]);
		}
		
		//Add the last coconut color
		coconutList[9] = newCoconutColor;
	}
	
	public int GetCoconutListSize()
	{
		return coconutListSize;
	}
}