using UnityEngine;
using System.Collections;

public class scoreController : MonoBehaviour {
	
	int score;
	public TextMesh scoreDisplay;
	public bool shouldDisplay, gameOver;
	
	public void IncreaseScore()
	{
		score++;
	}
	public void DecreaseScore()
	{
		score--;
	}
	
	void Update () {
		if(shouldDisplay)
		{
			scoreDisplay.text = "Score : " + score.ToString();
			if(gameOver)
				scoreDisplay.text = "Final Score: " + score.ToString();
		}
		else
			scoreDisplay.text = " ";
	}
}
