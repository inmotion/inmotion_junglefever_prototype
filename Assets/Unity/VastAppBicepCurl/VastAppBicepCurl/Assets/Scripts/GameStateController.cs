using UnityEngine;
using System.Collections;

public class GameStateController : MonoBehaviour {
	
	public GameObject Basket;
	public Transform redBasketPos, blueBasketPos;
	enum GAMESTATE
	{
		CALIBRATION,
		GAME
	};
	
	GAMESTATE gameState;
	
	void Start () {
		gameState = GAMESTATE.CALIBRATION;
		
	}
	
	public bool IsInGamePay()
	{
		return (gameState == GAMESTATE.GAME);
	}
	public void GameOver()
	{
		GameObject.Find("Score").GetComponent<scoreController>().gameOver = true;
		if(GameObject.Find("GUIParent") != null)	Destroy(GameObject.Find("GUIParent"));
		if(GameObject.Find("Platform") != null)		Destroy(GameObject.Find("Platform"));
		if(GameObject.Find("timerDisplay") != null) Destroy(GameObject.Find("timerDisplay"));
		if(GameObject.Find("BasketBlue") != null)	Destroy(GameObject.Find("BasketBlue"));
		if(GameObject.Find("BasketBlue") != null)	Destroy(GameObject.Find("BasketRed"));
		if(GameObject.Find("Coconut") != null)		Destroy(GameObject.Find("Coconut"));
		if(GameObject.Find("GameTimer") != null)	Destroy(GameObject.Find("GameTimer"));
	}
	public void EndCalibrationStartGame()
	{
		gameState = GAMESTATE.GAME;
		
		//Create the game related objects
		
		//blue basket
		GameObject bb = (GameObject) GameObject.Instantiate(Basket, blueBasketPos.position, Quaternion.identity);
		bb.AddComponent<Basket>();
		bb.renderer.material.color = Color.blue;
		bb.name = "BasketBlue";
		
		//red basket
		GameObject rb = (GameObject) GameObject.Instantiate(Basket, redBasketPos.position, Quaternion.identity);
		rb.AddComponent<Basket>();
		rb.renderer.material.color = Color.red;
		rb.name = "BasketRed";
		
		//platform
		GameObject plat = GameObject.Find("Platform");
		plat.renderer.enabled = true;
		plat.AddComponent<Platform>();
		
		//basket selector
		BasketSelector bs = GameObject.Find("BasketSelector").GetComponent<BasketSelector>();
		bs.basketBlue = bb.GetComponent<Basket>();
		bs.basketRed = rb.GetComponent<Basket>();
		bs.myPlatform = plat.GetComponent<Platform>();
		bs.isPlatformThere = true;
		
		//GUI Display
		GameObject.Find("timerDisplay").renderer.enabled = true;
		for(int i = 1; i < 6; i++)
			GameObject.Find( "renderer" + (i ) ).renderer.enabled = true;
		GameObject.Find("SelectedRenderer").renderer.enabled = true;
		CoconutDirector coco = GameObject.Find ("CoconutDirector").GetComponent<CoconutDirector>();
		coco.shouldUpdate = true;
		coco.name = "Coconut";
		coco.startTime = 5f;
		
		//Score
		scoreController sc = GameObject.Find ("Score").GetComponent<scoreController>();
		sc.scoreDisplay = GameObject.Find("ScoreDisplay").GetComponent<TextMesh>();
		sc.shouldDisplay = true;
		
		//GameTimer
		GameTimer gt = GameObject.Find("GameTimer").GetComponent<GameTimer>();
		gt.shouldUpdate = true;
	}
}
