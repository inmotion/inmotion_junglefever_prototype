using UnityEngine;
using System.Collections;

public class calibration : MonoBehaviour
{	
	public Collider player;
	public Material greenMaterial, whiteMaterial;
	public bool isWithin;
	public GameObject startPos;
	
	void Start ()
	{
		renderer.material = whiteMaterial;
	}
	
	void Update ()
	{
		if( player.bounds.min.x > collider.bounds.min.x && 
			player.bounds.max.x < collider.bounds.max.x &&
			player.bounds.min.y > collider.bounds.min.y &&
			player.bounds.max.y < collider.bounds.max.y &&
			player.gameObject.activeSelf == true)
		{
			
			renderer.material = greenMaterial;
			isWithin = true;
		}
		else
		{
			isWithin = false;
			renderer.material = whiteMaterial;
		}
	}
}
